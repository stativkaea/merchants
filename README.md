## Available Scripts

### `npm start`

Runs the app in the development mode.<br>

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>

### `npm run deploy`

Builds the app and deploys to GitHub pages.<br>

## Some considerations on technologies and approaches

### Static typing
TypeScript was preferred over Flow due to its great toolchain, community support, and compilation speed but Flow also can be a great choice because of its "soundness" approach.

### Many "connected" components
Container components correspond with respective routes and may be connected to the Redux store. In the same time other (not container) components may be connected to the store as well.
This approach results in some performance benefits (no need to re-render intermediary components which just take some props from the parent and pass them to the child) and also allows avoiding multi-level props passing so that these components can be moved around the components tree quite freely.
In some way re-usability of connected components is reduced but as Semantic UI is used as building blocks for UI this problem is not significant.

### Redux Saga
Redux Saga is used to handle the application side effects. The main reasons for that are (1) clean synchronous-like code, (2) error handling and (3) testability. Another great alternative is redux-observable.

### Reselect
Reselect is used for memoized selectors for performance reasons.

### Normalizr
Normalizr is used to normalize the state (flatten it).

### Redux-form
Redux-form is used to manage the state of form and their validation.

### Tests
I believe that the most effective approach is to cover application logic (reducers, selectors, sagas etc.) with unit tests and UI (components) should be tested mostly by integration tests. In our case, I picked up [Cypress](https://github.com/stativka/merchants-cypress-e2e). 
We can also use Jest and Enzyme for testing of components but I think integration tests with Cypress will give more confidence.
Unfortunately due to a lack of time the codebase test coverage is quite low. Provided tests should be treated more as an example of how respective modules should be tested.
