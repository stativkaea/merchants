import * as React from 'react';
import { SFC } from 'react';
import { WrappedFieldProps } from 'redux-form';
import { Input } from 'semantic-ui-react';

export const OptionalTextInput: SFC<WrappedFieldProps> = ({ input, meta: { error, invalid, touched } }) => (
  <Input error={touched && invalid} {...input} />
);
