import * as React from 'react';
import { Fragment, MouseEvent, SFC } from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { Icon, Menu, MenuItemProps } from 'semantic-ui-react';

import { RootState } from '../../../common/reducers';
import { SortingOrder } from '../../../common/types';
import { MerchantsActions } from '../../actions';
import { MerchantsSelectors } from '../../selectors';
import { SortBidsBy } from '../../types';

interface OwnProps extends Readonly<{}> {}

interface StateProps
  extends Readonly<{
      by: SortBidsBy;
      order: SortingOrder;
    }> {}

interface DispatchProps
  extends Readonly<{
      sortBidsBy: (event: MouseEvent, { name: by }: MenuItemProps) => void;
    }> {}

type BidsSortControlsProps = StateProps & DispatchProps;

const mapStateToPros: MapStateToProps<StateProps, OwnProps, RootState> = state => ({
  by: MerchantsSelectors.getSortBidsBy(state),
  order: MerchantsSelectors.getSortBidsOrder(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = dispatch => ({
  sortBidsBy(event: MouseEvent, { name }: MenuItemProps): void {
    const by: SortBidsBy = SortBidsBy.mapTo(name);
    dispatch(MerchantsActions.ActionCreators.bidsSort(by));
  },
});

const items = [SortBidsBy.Created, SortBidsBy.Amount, SortBidsBy.CarTitle];

const BidsSortControlsComponent: SFC<BidsSortControlsProps> = ({ by, order, sortBidsBy }) => {
  return (
    <Menu text={true} vertical={true} size="tiny">
      <Menu.Item key="header" header={true}>
        Sort By
      </Menu.Item>
      {items.map(item => (
        <Menu.Item
          key={item}
          name={item}
          active={by === item}
          onClick={sortBidsBy}
          content={
            <Fragment>
              {item}
              {by === item && (
                <Icon color="grey" name={order === SortingOrder.Ascending ? 'triangle up' : 'triangle down'} />
              )}
            </Fragment>
          }
        />
      ))}
    </Menu>
  );
};

export const BidsSortControls = connect(
  mapStateToPros,
  mapDispatchToProps
)(BidsSortControlsComponent);
