import * as React from 'react';
import { ReactNode } from 'react';
import { WrappedFieldProps } from 'redux-form';
import { Checkbox } from 'semantic-ui-react';

interface FormCheckboxProps
  extends WrappedFieldProps,
    Readonly<{
      label?: string;
    }> {}

export class FormCheckbox extends React.Component<FormCheckboxProps> {
  public render(): ReactNode {
    const {
      input: { value },
      label = '',
    } = this.props;
    return <Checkbox defaultChecked={!!value} label={label} onChange={this.handleChange} />;
  }

  private handleChange = (): void => {
    const {
      input: { value, onChange },
    } = this.props;
    onChange(!value);
  };
}
