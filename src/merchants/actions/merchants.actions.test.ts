import { ActionWithPayload } from '../../common/utils';
import { MerchantsAPI } from '../api';
import { merchantToAdd } from '../test-utils';
import { MerchantsActions } from './merchants.actions';

describe('Merchants action creators', () => {
  it('should create an action to add a merchant', () => {
    const expectedAction: ActionWithPayload<MerchantsActions.Types.Add, MerchantsAPI.PostOneProps> = {
      payload: merchantToAdd,
      type: MerchantsActions.Types.Add,
    };

    expect(MerchantsActions.ActionCreators.add(merchantToAdd)).toEqual(expectedAction);
  });
});
