import * as React from 'react';
import { SFC } from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { FormSubmitHandler } from 'redux-form';
import { Modal } from 'semantic-ui-react';

import { RootState } from '../../../../common/reducers';
import { MerchantsActions } from '../../../actions';
import { MerchantsAPI } from '../../../api';
import { MerchantsSelectors } from '../../../selectors';
import { MerchantsForm } from '../../merchants-form';

interface OwnProps extends Readonly<{}> {}

interface StateProps
  extends Readonly<{
      merchantAdditionInProgress: boolean;
    }> {}

interface DispatchProps
  extends Readonly<{
      add: FormSubmitHandler<MerchantsAPI.PostOneProps>;
      addCancel: () => void;
    }> {}

type AddModalProps = StateProps & DispatchProps;

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = state => ({
  merchantAdditionInProgress: MerchantsSelectors.getAdditionInProgress(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = dispatch => ({
  add(merchant: MerchantsAPI.PostOneProps): void {
    dispatch(MerchantsActions.ActionCreators.add(merchant));
  },
  addCancel(): void {
    dispatch(MerchantsActions.ActionCreators.addCancel());
  },
});

const EditModalComponent: SFC<AddModalProps> = ({ add, addCancel, merchantAdditionInProgress }) => {
  return !merchantAdditionInProgress ? null : (
    <Modal
      closeIcon="close"
      closeOnDimmerClick={false}
      closeOnEscape={false}
      content={<MerchantsForm onSubmit={add} />}
      header="Add new merchant"
      onClose={addCancel}
      open={true}
      size="small"
    />
  );
};

export const AddMerchantModal = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditModalComponent);
