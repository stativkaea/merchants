import { createSelector } from 'reselect';

import { RootState } from '../../common/reducers';
import { getEntityById } from '../../common/utils/get-entity-by-id';
import { Bid } from '../models';

export namespace MerchantsSelectors {
  const getState = (state: RootState) => state.merchants;

  const getEntities = createSelector(getState, state => state.entities.merchants);

  const getIds = createSelector(getState, state => state.result);

  const getEditMerchantId = createSelector(getState, state => state.editMerchantId);

  const getRemoveMerchantId = createSelector(getState, state => state.removeMerchantId);

  export const getShowBidsMerchantId = createSelector(getState, state => state.showBidsMerchantId);

  const getShowBidsMerchant = createSelector(getEntities, getShowBidsMerchantId, getEntityById);

  const getBidEntities = createSelector(getState, state => state.entities.bids);

  export const getPagination = createSelector(getState, state => state.pagination);

  export const getSortBidsBy = createSelector(getState, state => state.sortBidsBy);

  export const getSortBidsOrder = createSelector(getState, state => state.sortBidsOrder);

  export const getAdditionInProgress = createSelector(getState, state => state.merchantAdditionInProgress);

  export const getIsFetching = createSelector(getState, state => state.isFetching);

  export const getCount = createSelector(getIds, ids => ids.length);

  export const getIdsPaginated = createSelector(getIds, getPagination, (ids, { currentPage, pageSize }) =>
    ids.slice((currentPage - 1) * pageSize, currentPage * pageSize)
  );

  export const getMerchants = createSelector(getEntities, getIdsPaginated, (entities, ids) =>
    ids.map(id => entities[id])
  );

  export const getMerchantByIdFactory = (id: string) => createSelector(getEntities, entities => entities[id]);

  export const getEditMerchant = createSelector(getEntities, getEditMerchantId, getEntityById);

  export const getRemoveMerchant = createSelector(getEntities, getRemoveMerchantId, getEntityById);

  export const getBids = createSelector(
    getBidEntities,
    getShowBidsMerchant,
    getSortBidsBy,
    getSortBidsOrder,
    (entities, merchant, sortBy, sortOrder) =>
      !merchant ? [] : merchant.bids.map(bidId => entities[bidId]).sort(Bid.compareFnFactory(sortBy, sortOrder))
  );
}
