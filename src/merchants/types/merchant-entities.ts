import { Bid, Merchant } from '../models';

export interface MerchantEntities
  extends Readonly<{
      entities: {
        merchants: { [merchantId: string]: Merchant };
        bids: { [bidId: string]: Bid };
      };
      result: ReadonlyArray<string>;
    }> {}
