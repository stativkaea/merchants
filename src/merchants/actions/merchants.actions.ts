import { ActionsUnion } from '../../common/types';
import { Action, ActionWithPayload, createAction } from '../../common/utils';
import { MerchantsAPI } from '../api';
import { MerchantEntities, SortBidsBy } from '../types';

export namespace MerchantsActions {
  export const enum Types {
    Add = '[Merchants] Add',
    AddCancel = '[Merchants] Add Cancel',
    AddFail = '[Merchants] Add Fail',
    AddStart = '[Merchants] Add Start',
    AddSuccess = '[Merchants] Add Success',

    BidsHide = '[Merchants] Bids Hide',
    BidsShow = '[Merchants] Bids Show',
    BidsSort = '[Merchants] Bids Sort',

    Edit = '[Merchants] Edit',
    EditCancel = '[Merchants] Edit Cancel',
    EditFail = '[Merchants] Edit Fail',
    EditStart = '[Merchants] Edit Start',
    EditSuccess = '[Merchants] Edit Success',

    Fetch = '[Merchants] Fetch',
    FetchFail = '[Merchants] Fetch Fail',
    FetchSuccess = '[Merchants] Fetch Success',

    GoToPage = '[Merchants] Go To Page',

    Remove = '[Merchants] Remove',
    RemoveCancel = '[Merchants] Remove Cancel',
    RemoveFail = '[Merchants] Remove Fail',
    RemoveStart = '[Merchants] Remove Start',
    RemoveSuccess = '[Merchants] Remove Success',
  }

  export type Add = ActionWithPayload<Types.Add, MerchantsAPI.PostOneProps>;
  export type AddCancel = Action<Types.AddCancel>;
  export type AddFail = ActionWithPayload<Types.AddFail, Error>;
  export type AddStart = Action<Types.AddStart>;
  export type AddSuccess = ActionWithPayload<Types.AddSuccess, MerchantEntities>;

  export type BidsHide = Action<Types.BidsHide>;
  export type BidsShow = ActionWithPayload<Types.BidsShow, string>;
  export type BidsSort = ActionWithPayload<Types.BidsSort, SortBidsBy>;

  export type Edit = ActionWithPayload<Types.Edit, MerchantsAPI.PutOneProps>;
  export type EditCancel = Action<Types.EditCancel>;
  export type EditFail = ActionWithPayload<Types.EditFail, Error>;
  export type EditStart = ActionWithPayload<Types.EditStart, string>;
  export type EditSuccess = ActionWithPayload<Types.EditSuccess, MerchantsAPI.PutOneProps>;

  export type Fetch = Action<Types.Fetch>;
  export type FetchFail = ActionWithPayload<Types.FetchFail, Error>;
  export type FetchSuccess = ActionWithPayload<Types.FetchSuccess, MerchantEntities>;

  export type GoToPage = ActionWithPayload<Types.GoToPage, number>;

  export type Remove = ActionWithPayload<Types.Remove, string>;
  export type RemoveCancel = Action<Types.RemoveCancel>;
  export type RemoveFail = ActionWithPayload<Types.RemoveFail, Error>;
  export type RemoveStart = ActionWithPayload<Types.RemoveStart, string>;
  export type RemoveSuccess = ActionWithPayload<Types.RemoveSuccess, string>;

  export const ActionCreators = {
    add: (merchant: MerchantsAPI.PostOneProps): Add => createAction(Types.Add, merchant),
    addCancel: (): AddCancel => createAction(Types.AddCancel),
    addFail: (error: Error): AddFail => createAction(Types.AddFail, error),
    addStart: (): AddStart => createAction(Types.AddStart),
    addSuccess: (entities: MerchantEntities): AddSuccess => createAction(Types.AddSuccess, entities),

    bidsHide: (): BidsHide => createAction(Types.BidsHide),
    bidsShow: (merchantId: string): BidsShow => createAction(Types.BidsShow, merchantId),
    bidsSort: (sortBidsBy: SortBidsBy): BidsSort => createAction(Types.BidsSort, sortBidsBy),

    edit: (updatedProps: MerchantsAPI.PutOneProps): Edit => createAction(Types.Edit, updatedProps),
    editCancel: (): EditCancel => createAction(Types.EditCancel),
    editFail: (error: Error): EditFail => createAction(Types.EditFail, error),
    editStart: (merchantId: string): EditStart => createAction(Types.EditStart, merchantId),
    editSuccess: (updatedProps: MerchantsAPI.PutOneProps): EditSuccess => createAction(Types.EditSuccess, updatedProps),

    fetch: (): Fetch => createAction(Types.Fetch),
    fetchFail: (error: Error): FetchFail => createAction(Types.FetchFail, error),
    fetchSuccess: (entities: MerchantEntities): FetchSuccess => createAction(Types.FetchSuccess, entities),

    goToPage: (page: number): GoToPage => createAction(Types.GoToPage, page),

    remove: (merchantId: string): Remove => createAction(Types.Remove, merchantId),
    removeCancel: (): RemoveCancel => createAction(Types.RemoveCancel),
    removeFail: (error: Error): RemoveFail => createAction(Types.RemoveFail, error),
    removeStart: (merchantId: string): RemoveStart => createAction(Types.RemoveStart, merchantId),
    removeSuccess: (merchantId: string): RemoveSuccess => createAction(Types.RemoveSuccess, merchantId),
  };

  export type Actions = ActionsUnion<typeof ActionCreators>;
}
