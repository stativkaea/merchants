import * as React from 'react';
import { SFC } from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { Header, Table } from 'semantic-ui-react';

import { RootState } from '../../../common/reducers';
import { formatDate } from '../../../common/utils';
import { formatToEuro } from '../../../common/utils';
import { Bid } from '../../models';
import { MerchantsSelectors } from '../../selectors';
import { BidsSortControls } from '../bids-sort-controls';

interface StateProps
  extends Readonly<{
      bids: ReadonlyArray<Bid>;
    }> {}

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = state => ({
  bids: MerchantsSelectors.getBids(state),
});

const BidsTableComponent: SFC<StateProps> = ({ bids }) =>
  !bids.length ? null : (
    <Table basic="very" collapsing={true}>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Bid</Table.HeaderCell>
          <Table.HeaderCell>Amount</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {bids.map(bid => (
          <Table.Row key={bid.id}>
            <Table.Cell>
              <Header as="h4">
                <Header.Content>
                  {bid.carTitle}
                  <Header.Subheader>{formatDate(bid.created)}</Header.Subheader>
                </Header.Content>
              </Header>
            </Table.Cell>
            <Table.Cell>{formatToEuro(bid.amount)}</Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>

      {bids.length > 1 && (
        <Table.Footer>
          <Table.Row>
            <Table.Cell width={2}>
              <BidsSortControls />
            </Table.Cell>
          </Table.Row>
        </Table.Footer>
      )}
    </Table>
  );

export const BidsTable = connect(mapStateToProps)(BidsTableComponent);
