import { merchantsResponse, normalizedMerchantsResponse } from '../test-utils/merchants-response';
import { normalizeMerchants } from './merchants.normalizers';

describe('Merchants normalizers', () => {
  describe('normalizeMerchants', () => {
    it(`should normalize API's response`, () => {
      expect(normalizeMerchants(merchantsResponse)).toEqual(normalizedMerchantsResponse);
    });
  });
});
