import * as React from 'react';
import { Component, Fragment, ReactNode } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { Grid, Header } from 'semantic-ui-react';
import { RootState } from '../../common/reducers';
import { MerchantsActions } from '../actions';
import { BidsTable } from '../components/bids-table';
import { MerchantsTable } from '../components/merchnats-table';
import { MerchantsModals } from '../components/modals/merchants-modals.component';
import { Merchant } from '../models';
import { MerchantsSelectors } from '../selectors';

interface StateProps
  extends Readonly<{
      merchants: ReadonlyArray<Merchant>;
    }> {}

interface DispatchProps
  extends Readonly<{
      onMount: () => void;
    }> {}

interface MerchantsProps extends StateProps, DispatchProps {}

const mapStateToProps = (state: RootState): StateProps => ({
  merchants: MerchantsSelectors.getMerchants(state),
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
  onMount(): void {
    dispatch(MerchantsActions.ActionCreators.fetch());
  },
});

class Merchants extends Component<MerchantsProps> {
  public componentDidMount(): void {
    this.props.onMount();
  }

  public render(): ReactNode {
    return (
      <Fragment>
        <Grid className="merchants-container" columns={2}>
          <Grid.Row>
            <Grid.Column width={12}>
              <Header size="medium">Merchants</Header>
              <MerchantsTable />
            </Grid.Column>
            <Grid.Column width={4}>
              <BidsTable />
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <MerchantsModals />
      </Fragment>
    );
  }
}

export const MerchantsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Merchants);
