import * as React from 'react';
import { MouseEvent, SFC } from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { Button, Icon, Label, Pagination, PaginationProps, Segment, Table } from 'semantic-ui-react';
import { RootState } from '../../../common/reducers';
import { PaginationConfig } from '../../../common/types';
import { MerchantsActions } from '../../actions';
import { MerchantsSelectors } from '../../selectors';
import { MerchantsTableRow } from '../merchants-table-row';

import './merchants-table.css';

interface StateProps
  extends Readonly<{
      isFetching: boolean;
      merchantsCount: number;
      merchantsIds: ReadonlyArray<string>;
      pagination: PaginationConfig;
    }> {}

interface DispatchProps
  extends Readonly<{
      addStart: () => void;
      goToPage: (e: MouseEvent<HTMLAnchorElement>, { activePage }: PaginationProps) => void;
    }> {}

interface MerchantsTableProps extends StateProps, DispatchProps {}

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = state => ({
  isFetching: MerchantsSelectors.getIsFetching(state),
  merchantsCount: MerchantsSelectors.getCount(state),
  merchantsIds: MerchantsSelectors.getIdsPaginated(state),
  pagination: MerchantsSelectors.getPagination(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = dispatch => ({
  addStart(): void {
    dispatch(MerchantsActions.ActionCreators.addStart());
  },
  goToPage(event: MouseEvent<HTMLAnchorElement>, { activePage = 1 }): void {
    const page = typeof activePage === 'string' ? parseInt(activePage, 10) : activePage;
    dispatch(MerchantsActions.ActionCreators.goToPage(page));
  },
});

export const MerchantsTableComponent: SFC<MerchantsTableProps> = ({
  addStart,
  goToPage,
  isFetching,
  merchantsCount,
  merchantsIds,
  pagination,
}) => {
  return (
    <Segment loading={isFetching} padded={true}>
      <Table className="merchants-table" singleLine={true}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>E-mail</Table.HeaderCell>
            <Table.HeaderCell>Phone</Table.HeaderCell>
            <Table.HeaderCell>Bids</Table.HeaderCell>
            <Table.HeaderCell>Manage</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {merchantsIds.map(id => (
            <MerchantsTableRow key={id} id={id} />
          ))}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan="5">
              <Pagination
                activePage={pagination.currentPage}
                onPageChange={goToPage}
                totalPages={Math.ceil(merchantsCount / pagination.pageSize)}
              />

              <Label className="merchants-table__count-label">
                {pagination.pageSize * (pagination.currentPage - 1) + 1}
                &nbsp;-&nbsp;
                {Math.min(merchantsCount, pagination.pageSize * pagination.currentPage)}
                &nbsp;/&nbsp;
                {merchantsCount}
              </Label>

              <Button
                data-locator="add-merchant-button"
                floated="right"
                icon={true}
                labelPosition="left"
                onClick={addStart}
                primary={true}
                size="large"
              >
                <Icon name="user" /> Add User
              </Button>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    </Segment>
  );
};

export const MerchantsTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(MerchantsTableComponent);
