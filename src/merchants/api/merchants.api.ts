import { Omit } from 'react-redux';
import * as uuid from 'uuid';

import { Merchant } from '../models';
import { normalizeMerchants } from '../normalizers';
import { MerchantEntities } from '../types';
import { MOCK_MERCHANTS } from './mock';

type OmitBids = Omit<Merchant, 'bids'>;
type OmitBidsAndId = Omit<OmitBids, 'id'>;

// Mock API service
export namespace MerchantsAPI {
  export function getAll(): Promise<MerchantEntities> {
    return new Promise<MerchantEntities>(res => {
      setTimeout(() => res(normalizeMerchants(MOCK_MERCHANTS)), Math.random() * 3000);
    });
  }

  export type PostOneProps = OmitBidsAndId;

  export function postOne(merchant: PostOneProps): Promise<MerchantEntities> {
    return new Promise<MerchantEntities>(res =>
      setTimeout(
        () =>
          res(
            normalizeMerchants([
              {
                ...merchant,
                bids: [],
                id: uuid.v4(),
              },
            ])
          ),
        Math.random() * 1000
      )
    );
  }

  export type PutOneProps = OmitBids;

  export function putOne(merchant: PutOneProps): Promise<PutOneProps> {
    return new Promise<PutOneProps>(res => {
      setTimeout(() => res(merchant), Math.random() * 1000);
    });
  }

  export type DeleteOneProps = string;

  export function deleteOne(merchantId: string): Promise<string> {
    return new Promise<string>(res => {
      setTimeout(() => res(merchantId), Math.random() * 1000);
    });
  }
}
