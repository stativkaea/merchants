type FunctionType = (...args: Array<unknown>) => unknown;

interface ActionCreatorsMapObject {
  readonly [actionCreator: string]: FunctionType;
}

export type ActionsUnion<A extends ActionCreatorsMapObject> = ReturnType<A[keyof A]>;
