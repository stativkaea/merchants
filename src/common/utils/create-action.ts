export interface Action<TypeT extends string> {
  readonly type: TypeT;
}

export interface ActionWithPayload<TypeT extends string, PayloadT> extends Action<TypeT> {
  readonly payload: PayloadT;
}

/**
 * Strictly-typed generic action creator
 * 2 overloads for (1) simple action and (2) action with payload
 */
export function createAction<TypeT extends string>(type: TypeT): Action<TypeT>;
export function createAction<TypeT extends string, PayloadT>(
  type: TypeT,
  payload: PayloadT
): ActionWithPayload<TypeT, PayloadT>;
export function createAction<TypeT extends string, PayloadT>(type: TypeT, payload?: PayloadT) {
  return payload === undefined ? { type } : { type, payload };
}
