import { Omit } from 'react-redux';

import { Bid } from './bid';

export interface MerchantRawResponse
  extends Readonly<{
      avatarUrl: string | null;
      bids: ReadonlyArray<Bid>;
      email: string;
      firstname: string;
      hasPremium: boolean;
      id: string;
      lastname: string;
      phone: string;
    }> {}

export type Merchant = Omit<MerchantRawResponse, 'bids'> & { bids: ReadonlyArray<string> };
