import { assertNever } from '../utils';

export enum SortingOrder {
  Ascending = 'ascending',
  Descending = 'descending',
}

export namespace SortingOrder {
  export function toggle(order: SortingOrder) {
    switch (order) {
      case SortingOrder.Ascending:
        return SortingOrder.Descending;
      case SortingOrder.Descending:
        return SortingOrder.Ascending;
      default:
        return assertNever(order);
    }
  }
}
