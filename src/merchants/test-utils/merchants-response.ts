import { MerchantRawResponse } from '../models';
import { MerchantEntities } from '../types';

export const merchantsResponse: ReadonlyArray<MerchantRawResponse> = [
  {
    avatarUrl: 'https://randomuser.me/api/portraits/men/2.jpg',
    bids: [],
    email: 'odessa.allen@gmail.com',
    firstname: 'Odessa',
    hasPremium: true,
    id: '5b80f395cfb4fd2a1ed3eec9',
    lastname: 'Allen',
    phone: '+1 (962) 529-2386',
  },
  {
    avatarUrl: 'https://randomuser.me/api/portraits/men/1.jpg',
    bids: [
      {
        amount: 8020,
        carTitle: 'Audi Q5',
        created: 'Saturday, May 30, 2015 8:04 PM',
        id: '5b80f395cea2e3b4c8a78e76',
      },
      {
        amount: 27749,
        carTitle: 'Audi A1',
        created: 'Thursday, October 26, 2017 5:04 PM',
        id: '5b80f3950ed0eebfe0577877',
      },
    ],
    email: 'grimes.collier@gmail.com',
    firstname: 'Grimes',
    hasPremium: false,
    id: '5b80f395e240491cb0d8553b',
    lastname: 'Collier',
    phone: '+1 (935) 470-2197',
  },
];

export const normalizedMerchantsResponse: MerchantEntities = {
  entities: {
    bids: {
      '5b80f3950ed0eebfe0577877': {
        amount: 27749,
        carTitle: 'Audi A1',
        created: 'Thursday, October 26, 2017 5:04 PM',
        id: '5b80f3950ed0eebfe0577877',
      },
      '5b80f395cea2e3b4c8a78e76': {
        amount: 8020,
        carTitle: 'Audi Q5',
        created: 'Saturday, May 30, 2015 8:04 PM',
        id: '5b80f395cea2e3b4c8a78e76',
      },
    },
    merchants: {
      '5b80f395cfb4fd2a1ed3eec9': {
        avatarUrl: 'https://randomuser.me/api/portraits/men/2.jpg',
        bids: [],
        email: 'odessa.allen@gmail.com',
        firstname: 'Odessa',
        hasPremium: true,
        id: '5b80f395cfb4fd2a1ed3eec9',
        lastname: 'Allen',
        phone: '+1 (962) 529-2386',
      },
      '5b80f395e240491cb0d8553b': {
        avatarUrl: 'https://randomuser.me/api/portraits/men/1.jpg',
        bids: ['5b80f395cea2e3b4c8a78e76', '5b80f3950ed0eebfe0577877'],
        email: 'grimes.collier@gmail.com',
        firstname: 'Grimes',
        hasPremium: false,
        id: '5b80f395e240491cb0d8553b',
        lastname: 'Collier',
        phone: '+1 (935) 470-2197',
      },
    },
  },
  result: ['5b80f395cfb4fd2a1ed3eec9', '5b80f395e240491cb0d8553b'],
};
