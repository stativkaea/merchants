// helper function for exhaustive switch
export function assertNever(x: never): never {
  throw new Error('Unexpected object: ' + x);
}
