import * as React from 'react';
import { SFC } from 'react';
import { Field, InjectedFormProps, reduxForm, WrappedFieldProps } from 'redux-form';
import { Button, Form } from 'semantic-ui-react';

import { FormCheckbox, OptionalTextInput, RequiredTextInput } from '../../../common/components/form-controls';
import { InputValidators } from '../../../common/utils';
import { MerchantsAPI } from '../../api';

import './merchants-form.css';

const minLength2 = InputValidators.minLength(2);
const maxLength32 = InputValidators.maxLength(32);
const nameValidators = [InputValidators.required, minLength2, maxLength32];
const emailValidators = [InputValidators.required, InputValidators.email];
const phoneValidators = [InputValidators.required, InputValidators.phone];
const avatarUrlValidators = [InputValidators.url];

const MerchantsFormComponent: SFC<InjectedFormProps<MerchantsAPI.PostOneProps>> = ({
  handleSubmit,
  pristine,
  invalid,
}) => {
  return (
    <Form onSubmit={handleSubmit} className="merchants-form">
      <Form.Field>
        <label>First Name</label>
        <Field name="firstname" component={RequiredTextInput} validate={nameValidators} />
      </Form.Field>
      <Form.Field>
        <label>Last Name</label>
        <Field name="lastname" component={RequiredTextInput} validate={nameValidators} />
      </Form.Field>
      <Form.Field>
        <label>E-mail</label>
        <Field name="email" component={RequiredTextInput} validate={emailValidators} />
      </Form.Field>
      <Form.Field>
        <label>Phone</label>
        <Field name="phone" component={RequiredTextInput} validate={phoneValidators} />
      </Form.Field>
      <Form.Field>
        <label>Avatar Url</label>
        <Field name="avatarUrl" component={OptionalTextInput} validate={avatarUrlValidators} />
      </Form.Field>
      <Form.Field inline={true}>
        <Field name="hasPremium" component={Checkbox} />
      </Form.Field>
      <Form.Field>
        <Button disabled={pristine || invalid} primary={true} type="submit">
          Submit
        </Button>
      </Form.Field>
    </Form>
  );
};

const Checkbox: SFC<WrappedFieldProps> = props => <FormCheckbox label="Has Premium" {...props} />;

export const MerchantsForm = reduxForm<MerchantsAPI.PostOneProps>({ form: 'merchants-form' })(MerchantsFormComponent);
