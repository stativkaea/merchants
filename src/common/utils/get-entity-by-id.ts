// helper function for reselect selectors
export function getEntityById<EntitiesT>(
  entities: EntitiesT,
  id: keyof EntitiesT | undefined
): EntitiesT[keyof EntitiesT] | undefined {
  return !!id ? entities[id] : undefined;
}
