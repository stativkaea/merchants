const formatter = new Intl.DateTimeFormat('en-GB', {
  day: '2-digit',
  month: 'long',
  year: 'numeric',
});

export function formatDate(date: string): string {
  return formatter.format(new Date(date));
}
