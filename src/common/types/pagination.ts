export interface PaginationConfig
  extends Readonly<{
      currentPage: number;
      pageSize: number;
    }> {}

export interface Paginated
  extends Readonly<{
      pagination: PaginationConfig;
    }> {}
