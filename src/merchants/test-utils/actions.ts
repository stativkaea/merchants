import { MerchantsActions } from '../actions';
import { merchantToAdd } from './merchant-to-add';

export const addAction = MerchantsActions.ActionCreators.add(merchantToAdd);
