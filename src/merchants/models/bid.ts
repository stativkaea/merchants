import { SortingOrder } from '../../common/types';
import { assertNever } from '../../common/utils';
import { SortBidsBy } from '../types';

export interface Bid
  extends Readonly<{
      amount: number;
      carTitle: string;
      created: string;
      id: string;
    }> {}

export namespace Bid {
  type BidsCompareFn = (prev: Bid, next: Bid) => number;

  export function compareFnFactory(by: SortBidsBy, order: SortingOrder): BidsCompareFn {
    switch (by) {
      case SortBidsBy.Created:
        return sortByDateFnFactory(order);
      case SortBidsBy.Amount:
        return sortByAmountFnFactory(order);
      case SortBidsBy.CarTitle:
        return sortByCarTitleFnFactory(order);
      default:
        return assertNever(by);
    }
  }

  function sortByDateFnFactory(order: SortingOrder): BidsCompareFn {
    switch (order) {
      case SortingOrder.Ascending:
        return (prev: Bid, next: Bid): number => {
          return new Date(prev.created).valueOf() - new Date(next.created).valueOf();
        };

      case SortingOrder.Descending:
        return (prev: Bid, next: Bid): number => {
          return new Date(next.created).valueOf() - new Date(prev.created).valueOf();
        };

      default:
        return assertNever(order);
    }
  }

  function sortByAmountFnFactory(order: SortingOrder): BidsCompareFn {
    switch (order) {
      case SortingOrder.Ascending:
        return (prev: Bid, next: Bid): number => {
          return prev.amount - next.amount;
        };

      case SortingOrder.Descending:
        return (prev: Bid, next: Bid): number => {
          return next.amount - prev.amount;
        };

      default:
        return assertNever(order);
    }
  }

  function sortByCarTitleFnFactory(order: SortingOrder): BidsCompareFn {
    switch (order) {
      case SortingOrder.Ascending:
        return (prev: Bid, next: Bid): number => {
          switch (true) {
            case prev.carTitle > next.carTitle:
              return 1;
            case prev.carTitle < next.carTitle:
              return -1;
            default:
              return 0;
          }
        };

      case SortingOrder.Descending:
        return (prev: Bid, next: Bid): number => {
          switch (true) {
            case prev.carTitle > next.carTitle:
              return -1;
            case prev.carTitle < next.carTitle:
              return 1;
            default:
              return 0;
          }
        };

      default:
        return assertNever(order);
    }
  }
}
