export namespace InputValidators {
  export function email(value: unknown): string | undefined {
    return typeof value === 'string' && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? 'Invalid email address'
      : undefined;
  }

  export function minLength(min: number): (value: unknown) => string | undefined {
    return value => (typeof value === 'string' && value.length < min ? `Must be ${min} characters or more` : undefined);
  }

  export function maxLength(max: number): (value: unknown) => string | undefined {
    return value => (typeof value === 'string' && value.length > max ? `Must be ${max} characters or less` : undefined);
  }

  export function phone(value: unknown): string | undefined {
    return typeof value === 'string' && !/^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g.test(value)
      ? 'Invalid phone number, must be 10 digits'
      : undefined;
  }

  export function required(value: unknown): string | undefined {
    return !!value ? undefined : 'Required';
  }
  export function url(value: unknown): string | undefined {
    return value &&
      typeof value === 'string' &&
      !/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i.test(
        value
      )
      ? 'Invalid URL'
      : undefined;
  }
}
