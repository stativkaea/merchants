import * as React from 'react';
import { SFC } from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { FormSubmitHandler } from 'redux-form';
import { Modal } from 'semantic-ui-react';

import { RootState } from '../../../../common/reducers';
import { MerchantsActions } from '../../../actions';
import { MerchantsAPI } from '../../../api';
import { Merchant } from '../../../models';
import { MerchantsSelectors } from '../../../selectors';
import { MerchantsForm } from '../../merchants-form';

interface OwnProps extends Readonly<{}> {}

interface StateProps
  extends Readonly<{
      merchant: Merchant | undefined;
    }> {}

interface DispatchProps
  extends Readonly<{
      edit: FormSubmitHandler<MerchantsAPI.PutOneProps>;
      editCancel: () => void;
    }> {}

type EditModalProps = StateProps & DispatchProps;

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = state => ({
  merchant: MerchantsSelectors.getEditMerchant(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = dispatch => ({
  edit(merchant: MerchantsAPI.PutOneProps): void {
    dispatch(MerchantsActions.ActionCreators.edit(merchant));
  },
  editCancel(): void {
    dispatch(MerchantsActions.ActionCreators.editCancel());
  },
});

const EditModalComponent: SFC<EditModalProps> = ({ edit, editCancel, merchant }) => {
  return !merchant ? null : (
    <Modal
      closeIcon="close"
      closeOnDimmerClick={false}
      closeOnEscape={false}
      content={<MerchantsForm onSubmit={edit} initialValues={merchant} />}
      header={`Edit profile of ${merchant.firstname}`}
      onClose={editCancel}
      open={true}
      size="small"
    />
  );
};

export const EditMerchantModal = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditModalComponent);
