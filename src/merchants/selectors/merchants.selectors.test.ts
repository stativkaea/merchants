import { RootState } from '../../common/reducers';
import { rootState } from '../../common/test-utils';
import { normalizedMerchantsResponse } from '../test-utils/merchants-response';
import { MerchantsSelectors } from './merchants.selectors';

describe('Merchants selectors', () => {
  describe('getMerchants', () => {
    it('should return an array of merchants corresponding to pagination state', () => {
      const state: RootState = {
        ...rootState,
        merchants: {
          ...rootState.merchants,
          ...normalizedMerchantsResponse,
        },
      };

      expect(MerchantsSelectors.getMerchants(state)).toEqual(
        Object.values(normalizedMerchantsResponse.entities.merchants)
      );
    });
  });
});
