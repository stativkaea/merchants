import { merchantsInitialState } from '../../merchants/test-utils/initial-state';
import { RootState } from '../reducers';

export const rootState: RootState = {
  merchants: merchantsInitialState,
};
