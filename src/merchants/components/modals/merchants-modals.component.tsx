import * as React from 'react';
import { Fragment, SFC } from 'react';

import { AddMerchantModal } from './add-merchant-modal';
import { EditMerchantModal } from './edit-merchant-modal';
import { RemoveMerchantModal } from './remove-merchant-modal';

export const MerchantsModals: SFC = () => {
  return (
    <Fragment>
      <AddMerchantModal />
      <EditMerchantModal />
      <RemoveMerchantModal />
    </Fragment>
  );
};
