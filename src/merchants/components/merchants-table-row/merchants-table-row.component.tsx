import * as React from 'react';
import { Component, ReactNode } from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { Button, Header, Icon, Image, Popup, Table } from 'semantic-ui-react';

import dummyAvatar from '../../../common/assets/dummy-avatar.png';
import { RootState } from '../../../common/reducers';
import { MerchantsActions } from '../../actions';
import { Merchant } from '../../models';
import { MerchantsSelectors } from '../../selectors';

interface OwnProps
  extends Readonly<{
      id: string;
    }> {}

interface StateProps
  extends Readonly<{
      merchant: Merchant;
      showBidsMerchantId: string | undefined;
    }> {}

interface DispatchProps
  extends Readonly<{
      bidsShow: () => void;
      bidsHide: () => void;
      editStart: () => void;
      removeStart: () => void;
    }> {}

type MerchantsTableRowProps = OwnProps & StateProps & DispatchProps;

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (state, { id }) => ({
  merchant: MerchantsSelectors.getMerchantByIdFactory(id)(state),
  showBidsMerchantId: MerchantsSelectors.getShowBidsMerchantId(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (dispatch, { id }) => ({
  bidsShow(): void {
    dispatch(MerchantsActions.ActionCreators.bidsShow(id));
  },
  bidsHide(): void {
    dispatch(MerchantsActions.ActionCreators.bidsHide());
  },
  editStart(): void {
    dispatch(MerchantsActions.ActionCreators.editStart(id));
  },
  removeStart(): void {
    dispatch(MerchantsActions.ActionCreators.removeStart(id));
  },
});

export class MerchantsTableRowComponent extends Component<MerchantsTableRowProps> {
  public render(): ReactNode {
    const {
      bidsHide,
      bidsShow,
      editStart,
      id,
      merchant: { avatarUrl, bids, email, firstname, hasPremium, lastname, phone },
      removeStart,
      showBidsMerchantId,
    } = this.props;

    const avatarSrc = avatarUrl || dummyAvatar;
    const bidsAreShown = id === showBidsMerchantId;

    return (
      <Table.Row data-locator="merchants-table-row" key={id} warning={bidsAreShown}>
        <Table.Cell>
          <Header as="h4" image={true}>
            <Image rounded={true} size="mini" src={avatarSrc} />
            <Header.Content>
              {firstname} {lastname}
              {hasPremium && <Popup trigger={<Icon color="yellow" name="star" />}>Premium user</Popup>}
            </Header.Content>
          </Header>
        </Table.Cell>

        <Table.Cell data-locator="merchants-table-row-email">{email}</Table.Cell>

        <Table.Cell>{phone}</Table.Cell>

        <Table.Cell>
          {bids.length ? (
            bidsAreShown ? (
              <Button onClick={bidsHide} size="small">
                hide bids
              </Button>
            ) : (
              <Button onClick={bidsShow} size="small">{`show ${bids.length} bid${bids.length > 1 ? 's' : ''}`}</Button>
            )
          ) : (
            'no bids'
          )}
        </Table.Cell>

        <Table.Cell>
          <Button animated="fade" data-locator="edit-merchant-button" onClick={editStart}>
            <Button.Content hidden={true}>Edit</Button.Content>
            <Button.Content visible={true}>
              <Icon name="edit outline" />
            </Button.Content>
          </Button>

          <Button animated="fade" data-locator="remove-merchant-button" onClick={removeStart}>
            <Button.Content hidden={true}>Remove</Button.Content>
            <Button.Content visible={true}>
              <Icon name="remove user" />
            </Button.Content>
          </Button>
        </Table.Cell>
      </Table.Row>
    );
  }
}

export const MerchantsTableRow = connect(
  mapStateToProps,
  mapDispatchToProps
)(MerchantsTableRowComponent);
