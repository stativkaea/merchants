import { MerchantsActions } from '../actions';
import { merchantsInitialState } from '../test-utils/initial-state';
import { merchantsReducer } from './merchants.reducer';

describe('Merchants reducer', () => {
  it('should return initial state if neither state nor merchants action are passed', () => {
    expect(merchantsReducer(undefined, {} as MerchantsActions.Actions)).toEqual(merchantsInitialState);
  });
});
