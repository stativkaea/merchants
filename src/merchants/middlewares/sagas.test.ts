import { call, put, takeEvery } from 'redux-saga/effects';
import { cloneableGenerator } from 'redux-saga/utils';

import { MerchantsActions } from '../actions';
import { MerchantsAPI } from '../api';
import { addAction, merchantToAdd } from '../test-utils';
import { normalizedMerchantsResponse } from '../test-utils/merchants-response';
import { add, merchantsSaga } from './sagas';

describe('Merchants sagas', () => {
  it('should handle Add action', () => {
    const iterator = merchantsSaga();
    const effects = Array.from(iterator);

    expect(effects).toContainEqual(takeEvery(MerchantsActions.Types.Add, add));
  });

  describe('add saga', () => {
    const iteratorClone = cloneableGenerator(add)(addAction);

    it('should call MerchantsAPI add method with the merchant to add', () => {
      expect(iteratorClone.next().value).toEqual(call(MerchantsAPI.postOne, merchantToAdd));
    });

    it('should dispatch addSuccess action with received entities if no error occurs', () => {
      const clone = iteratorClone.clone();

      expect(clone.next(normalizedMerchantsResponse).value).toEqual(
        put(MerchantsActions.ActionCreators.addSuccess(normalizedMerchantsResponse))
      );
    });

    it('should dispatch addFail action with received error if the API call fails', () => {
      const clone = iteratorClone.clone();
      const error = new Error('API call fails');

      expect(clone.throw && clone.throw(error).value).toEqual(put(MerchantsActions.ActionCreators.addFail(error)));
    });
  });
});
