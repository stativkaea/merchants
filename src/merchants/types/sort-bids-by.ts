export enum SortBidsBy {
  Amount = 'amount',
  CarTitle = 'car title',
  Created = 'created',
}

export namespace SortBidsBy {
  export function mapTo(by?: string): SortBidsBy {
    switch (by) {
      case SortBidsBy.CarTitle:
        return SortBidsBy.CarTitle;
      case SortBidsBy.Amount:
        return SortBidsBy.Amount;
      default:
        return SortBidsBy.Created;
    }
  }
}
