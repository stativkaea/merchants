import * as React from 'react';
import { ReactNode } from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { Modal } from 'semantic-ui-react';

import { RootState } from '../../../../common/reducers';
import { MerchantsActions } from '../../../actions';
import { Merchant } from '../../../models';
import { MerchantsSelectors } from '../../../selectors';

interface OwnProps extends Readonly<{}> {}

interface StateProps
  extends Readonly<{
      merchant: Merchant | undefined;
    }> {}

interface DispatchProps
  extends Readonly<{
      remove: (id: string) => void;
      removeCancel: () => void;
    }> {}

type RemoveModalProps = StateProps & DispatchProps;

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = state => ({
  merchant: MerchantsSelectors.getRemoveMerchant(state),
});

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = dispatch => ({
  remove(id: string): void {
    dispatch(MerchantsActions.ActionCreators.remove(id));
  },
  removeCancel(): void {
    dispatch(MerchantsActions.ActionCreators.removeCancel());
  },
});

class RemoveModalComponent extends React.Component<RemoveModalProps> {
  public render(): ReactNode {
    const { merchant, removeCancel } = this.props;
    return !merchant ? null : (
      <Modal
        actions={[
          { color: 'red', content: 'No', key: 'no', onClick: removeCancel },
          { color: 'green', content: 'Yes', key: 'yes', onClick: this.remove },
        ]}
        content={`${merchant.firstname} ${merchant.lastname} will be completely removed from a list of merchants.`}
        header={`Are you sure you want to remove ${merchant.firstname}?`}
        open={true}
      />
    );
  }

  private remove = (): void => {
    const { merchant, remove } = this.props;

    if (merchant) {
      remove(merchant.id);
    }
  };
}

export const RemoveMerchantModal = connect(
  mapStateToProps,
  mapDispatchToProps
)(RemoveModalComponent);
