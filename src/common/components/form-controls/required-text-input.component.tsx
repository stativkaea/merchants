import * as React from 'react';
import { SFC } from 'react';
import { WrappedFieldProps } from 'redux-form';
import { Input } from 'semantic-ui-react';

const requiredFieldLabel = { icon: 'asterisk' };

export const RequiredTextInput: SFC<WrappedFieldProps> = ({ input, meta: { error, invalid, touched } }) => (
  <Input error={touched && invalid} label={requiredFieldLabel} labelPosition="right corner" {...input} />
);
