import { combineReducers, Reducer } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { merchantsReducer, MerchantsState } from '../../merchants/reducers';

export interface RootState
  extends Readonly<{
      merchants: MerchantsState;
    }> {}

export const rootReducer: Reducer<RootState> = combineReducers({
  form: formReducer,
  merchants: merchantsReducer,
});
