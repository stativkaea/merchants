import { SortingOrder } from '../../common/types';
import { MerchantsState } from '../reducers';
import { SortBidsBy } from '../types';

export const merchantsInitialState: MerchantsState = {
  editMerchantId: undefined,
  entities: {
    bids: {},
    merchants: {},
  },
  isFetching: false,
  merchantAdditionInProgress: false,
  pagination: {
    currentPage: 1,
    pageSize: 5,
  },
  removeMerchantId: undefined,
  result: [],
  showBidsMerchantId: undefined,
  sortBidsBy: SortBidsBy.Created,
  sortBidsOrder: SortingOrder.Ascending,
};
