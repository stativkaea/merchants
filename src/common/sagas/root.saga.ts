import { all } from 'redux-saga/effects';
import { merchantsSaga } from '../../merchants/middlewares';

export function* rootSaga() {
  yield all([merchantsSaga()]);
}
