import { Reducer } from 'redux';

import { SortingOrder } from '../../common/types';
import { Paginated } from '../../common/types';
import { MerchantsActions } from '../actions';
import { MerchantEntities, SortBidsBy } from '../types';

export interface MerchantsState
  extends Readonly<MerchantEntities>,
    Paginated,
    Readonly<{
      editMerchantId: string | undefined;
      isFetching: boolean;
      merchantAdditionInProgress: boolean;
      removeMerchantId: string | undefined;
      showBidsMerchantId: string | undefined;
      sortBidsBy: SortBidsBy;
      sortBidsOrder: SortingOrder;
    }> {}

const initialState: MerchantsState = {
  editMerchantId: undefined,
  entities: {
    bids: {},
    merchants: {},
  },
  isFetching: false,
  merchantAdditionInProgress: false,
  pagination: {
    currentPage: 1,
    pageSize: 5,
  },
  removeMerchantId: undefined,
  result: [],
  showBidsMerchantId: undefined,
  sortBidsBy: SortBidsBy.Created,
  sortBidsOrder: SortingOrder.Ascending,
};

export const merchantsReducer: Reducer<MerchantsState, MerchantsActions.Actions> = (
  state: MerchantsState = initialState,
  action: MerchantsActions.Actions
) => {
  switch (action.type) {
    case MerchantsActions.Types.Add:
      return { ...state, isFetching: true };

    case MerchantsActions.Types.AddCancel:
      return { ...state, merchantAdditionInProgress: false };

    case MerchantsActions.Types.AddFail:
      return { ...state, isFetching: false, merchantAdditionInProgress: false };

    case MerchantsActions.Types.AddStart:
      return { ...state, merchantAdditionInProgress: true };

    case MerchantsActions.Types.AddSuccess:
      return {
        ...state,
        entities: {
          bids: { ...state.entities.bids, ...action.payload.entities.bids },
          merchants: { ...state.entities.merchants, ...action.payload.entities.merchants },
        },
        isFetching: false,
        merchantAdditionInProgress: false,
        result: [...action.payload.result, ...state.result],
      };

    case MerchantsActions.Types.BidsHide:
      return { ...state, showBidsMerchantId: undefined };

    case MerchantsActions.Types.BidsShow:
      return { ...state, showBidsMerchantId: action.payload };

    case MerchantsActions.Types.BidsSort: {
      const order =
        state.sortBidsBy === action.payload ? SortingOrder.toggle(state.sortBidsOrder) : state.sortBidsOrder;
      return { ...state, sortBidsBy: action.payload, sortBidsOrder: order };
    }

    case MerchantsActions.Types.Edit:
      return { ...state, isFetching: true };

    case MerchantsActions.Types.EditCancel:
    case MerchantsActions.Types.EditFail:
      return { ...state, editMerchantId: undefined, isFetching: false };

    case MerchantsActions.Types.EditStart:
      return { ...state, editMerchantId: action.payload };

    case MerchantsActions.Types.EditSuccess:
      return {
        ...state,
        editMerchantId: undefined,
        entities: {
          ...state.entities,
          merchants: {
            ...state.entities.merchants,
            [action.payload.id]: { ...state.entities.merchants[action.payload.id], ...action.payload },
          },
        },
        isFetching: false,
      };

    case MerchantsActions.Types.Fetch:
      return { ...state, isFetching: true };

    case MerchantsActions.Types.FetchFail:
      return { ...state, isFetching: false };

    case MerchantsActions.Types.FetchSuccess:
      return { ...state, isFetching: false, ...action.payload };

    case MerchantsActions.Types.GoToPage: {
      return {
        ...state,
        pagination: { ...state.pagination, currentPage: action.payload },
        showBidsMerchantId: undefined,
      };
    }

    case MerchantsActions.Types.Remove:
      return { ...state, isFetching: true };

    case MerchantsActions.Types.RemoveCancel:
    case MerchantsActions.Types.RemoveFail:
      return { ...state, isFetching: false, removeMerchantId: undefined };

    case MerchantsActions.Types.RemoveStart:
      return { ...state, removeMerchantId: action.payload };

    case MerchantsActions.Types.RemoveSuccess:
      return {
        ...state,
        isFetching: false,
        removeMerchantId: undefined,
        result: state.result.filter(id => id !== action.payload),
      };
  }

  return state;
};
