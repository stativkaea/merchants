import { MerchantsAPI } from '../api';

export const merchantToAdd: MerchantsAPI.PostOneProps = {
  avatarUrl: null,
  email: 'john.doe@gmail.com',
  firstname: 'John',
  hasPremium: false,
  lastname: 'Doe',
  phone: '+380994814065',
};
