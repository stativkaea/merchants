import * as React from 'react';
import { Component, ReactNode } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { MerchantsContainer } from '../../../merchants/containers';
import { store } from '../../store';

import './app.css';

export class App extends Component {
  public render(): ReactNode {
    return (
      <div className="app">
        <Provider store={store}>
          <Router>
            <Route path="/" component={MerchantsContainer} />
          </Router>
        </Provider>
      </div>
    );
  }
}
