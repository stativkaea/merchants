import { SagaIterator } from 'redux-saga';
import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';

import { MerchantsActions } from '../actions';
import { MerchantsAPI } from '../api';
import { MerchantEntities } from '../types';

export function* merchantsSaga(): SagaIterator {
  yield takeEvery(MerchantsActions.Types.Add, add);
  yield takeEvery(MerchantsActions.Types.Edit, edit);
  yield takeLatest(MerchantsActions.Types.Fetch, fetch);
  yield takeEvery(MerchantsActions.Types.Remove, remove);
}

export function* add(action: MerchantsActions.Add): SagaIterator {
  try {
    const entities: MerchantEntities = yield call<MerchantsAPI.PostOneProps>(MerchantsAPI.postOne, action.payload);
    yield put(MerchantsActions.ActionCreators.addSuccess(entities));
  } catch (e) {
    yield put(MerchantsActions.ActionCreators.addFail(e));
  }
}

export function* edit(action: MerchantsActions.Edit): SagaIterator {
  try {
    const updatedProps: MerchantsAPI.PutOneProps = yield call<MerchantsAPI.PutOneProps>(
      MerchantsAPI.putOne,
      action.payload
    );
    yield put(MerchantsActions.ActionCreators.editSuccess(updatedProps));
  } catch (e) {
    yield put(MerchantsActions.ActionCreators.editFail(e));
  }
}

export function* fetch(): SagaIterator {
  try {
    const merchants: MerchantEntities = yield call(MerchantsAPI.getAll);
    yield put(MerchantsActions.ActionCreators.fetchSuccess(merchants));
  } catch (e) {
    yield put(MerchantsActions.ActionCreators.fetchFail(e));
  }
}

export function* remove(action: MerchantsActions.Remove): SagaIterator {
  try {
    const merchantId = yield call<MerchantsAPI.DeleteOneProps>(MerchantsAPI.deleteOne, action.payload);
    yield put(MerchantsActions.ActionCreators.removeSuccess(merchantId));
  } catch (e) {
    yield put(MerchantsActions.ActionCreators.removeFail(e));
  }
}
