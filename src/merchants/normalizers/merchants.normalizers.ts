import { normalize, schema } from 'normalizr';
import { MerchantRawResponse } from '../models';
import { MerchantEntities } from '../types';

const bid = new schema.Entity('bids');

const merchant = new schema.Entity('merchants', {
  bids: [bid],
});

export function normalizeMerchants(merchants: ReadonlyArray<MerchantRawResponse>): MerchantEntities {
  return normalize(merchants, [merchant]);
}
